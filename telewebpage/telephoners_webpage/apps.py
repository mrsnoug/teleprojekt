from django.apps import AppConfig


class TelephonersWebpageConfig(AppConfig):
    name = 'telephoners_webpage'
