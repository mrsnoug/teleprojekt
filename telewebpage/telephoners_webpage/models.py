from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
import re

# Create your models here.
class TeleNews(models.Model):
    news_title = models.CharField(max_length=256)
    # news_body = models.TextField()
    news_body = RichTextUploadingField()  # changing to rich text field
    news_published = models.DateTimeField("Date published")
    news_image_tag = RichTextUploadingField(null=True)
    slug = models.SlugField(default=1, unique=True)

    def __str__(self):
        return self.news_title


    def clean(self):
        self.news_body = self.news_body.replace("</p>\n<p>","").replace('<p>','').replace('</p>','').replace("\"","'").replace('\r','').replace('\n','\\n')
        self.news_image_tag = self.news_image_tag.replace("</p>\n<p>","").replace('<p>','').replace('</p>','').replace("\"","'").replace('\r','').replace('\n','\\n')
        self.news_body = re.sub("style='([^']*)'[^>]","",self.news_body)
        self.news_image_tag = re.sub("style='([^']*)'[^>]","",self.news_image_tag)
        

class TeleProject(models.Model):
    proj_name = models.CharField(max_length=256)
    proj_description = RichTextUploadingField()
    proj_short_description = models.TextField()
    slug = models.SlugField(default=1, unique=True)

    def __str__(self):
        return self.proj_short_description

    
    def clean(self):
        self.proj_description = self.proj_description.replace("</p>\n<p>","").replace('<p>','').replace('</p>','').replace("\"","'").replace("\n","\\n").rstrip("\r").replace("&nbsp", "")
        self.proj_description = re.sub("style='([^']*)'[^>]","",self.proj_description)
        self.proj_short_description = self.proj_short_description.replace("\r","").replace('\n','\\n')

