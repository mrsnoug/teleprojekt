from django.urls import path
from . import views


app_name = "telephoners_webpage"

urlpatterns = [
    path("", views.HomepageView.as_view()),
    path("about", views.AboutView.as_view()),
    path("history", views.HistoryView.as_view()),
    path("work_with_us", views.WorkwithusView.as_view()),
    path("projects", views.ProjectsView.as_view()),
    path("projects/<slug:slug>", views.SingleProjectView.as_view()),
    # path("Projekt_DroNET", views.ProjectView.as_view()),
    path("contact", views.ContactView.as_view()),
    path("login", views.LoginView.as_view()),
    path("news", views.NewsView.as_view()),
    path("news/<slug:slug>", views.SingleNewsView.as_view()),
    path("management", views.ManagementView.as_view()),
]
