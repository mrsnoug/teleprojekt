# Generated by Django 3.0.3 on 2020-06-08 12:22

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('telephoners_webpage', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='telenews',
            name='news_body',
            field=ckeditor_uploader.fields.RichTextUploadingField(),
        ),
        migrations.AlterField(
            model_name='teleproject',
            name='proj_description',
            field=ckeditor_uploader.fields.RichTextUploadingField(),
        ),
    ]
