from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin

from .models import TeleNews, TeleProject

import json


class HomepageView(View):
    template_name = "telephoners_webpage/landing_page.html"

    def get(self, request):
        news_data = [
            {
                "title": news.news_title,
                "body": news.news_body,
                "published": str(news.news_published),
                "image_tag": news.news_image_tag,
                "slug": news.slug,
            }
            for news in TeleNews.objects.all()
        ]

        projects_data = [
            {
                "name": project.proj_name,
                "desc": project.proj_description,
                "short_desc": project.proj_short_description,
                "slug": project.slug,
            }
            for project in TeleProject.objects.all()
        ]
        context = {"news": json.dumps(news_data), "projects": json.dumps(projects_data)}
        return render(request, self.template_name, context)


class AboutView(View):
    template_name = "telephoners_webpage/about.html"

    def get(self, request):
        return render(request, self.template_name)


class NewsView(View):
    template_name = "telephoners_webpage/news.html"

    def get(self, request):
        news_data = [
            {
                "title": news.news_title,
                "body": news.news_body,
                "published": str(news.news_published),
                "image_tag": news.news_image_tag,
                "slug": news.slug,
            }
            for news in TeleNews.objects.all()
        ]
        context = {"news": json.dumps(news_data)}
        return render(request, self.template_name, context)


class SingleNewsView(SingleObjectMixin, View):
    template_name = "telephoners_webpage/single_news.html"
    model = TeleNews

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        data = {
            "title": self.object.news_title,
            "body": self.object.news_body,
            "published": str(self.object.news_published),
            "image_tag": self.object.news_image_tag,
        }
        context = {"news": json.dumps(data)}
        return render(request, self.template_name, context)


class HistoryView(View):
    template_name = "telephoners_webpage/history.html"

    def get(self, request):
        return render(request, self.template_name)


class ManagementView(View):
    template_name = "telephoners_webpage/management.html"

    def get(self, request):
        return render(request, self.template_name)


class SingleProjectView(SingleObjectMixin, View):
    template_name = "telephoners_webpage/project.html"
    model = TeleProject

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        data = {
            "name": self.object.proj_name,
            "desc": self.object.proj_description,
            "short_desc": self.object.proj_short_description,
        }
        context = {"projects": json.dumps(data)}
        return render(request, self.template_name, context)


class ProjectsView(View):
    template_name = "telephoners_webpage/projects.html"

    def get(self, request):
        projects_data = [
            {
                "name": project.proj_name,
                "desc": project.proj_description,
                "short_desc": project.proj_short_description,
                "slug": project.slug,
            }
            for project in TeleProject.objects.all()
        ]
        context = {"projects": json.dumps(projects_data)}
        return render(request, self.template_name, context)


class WorkwithusView(View):
    template_name = "telephoners_webpage/work_with_us.html"

    def get(self, request):
        return render(request, self.template_name)


class LoginView(View):
    template_name = "telephoners_webpage/login.html"

    def get(self, request):
        return render(request, self.template_name)


class ContactView(View):
    template_name = "telephoners_webpage/contact.html"

    def get(self, request):
        return render(request, self.template_name)
