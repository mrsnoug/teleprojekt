from django.contrib import admin
from .models import TeleNews, TeleProject
from django.db import models


class Admin(admin.ModelAdmin):
    fields = ["news_title", "news_published", "news_body", "news_image_tag", "slug"]


class ProjectAdmin(admin.ModelAdmin):
    fields = ["proj_name", "proj_description", "proj_short_description", "slug"]


# Register your models here.


admin.site.register(TeleNews, Admin)
admin.site.register(TeleProject, ProjectAdmin)
