import React, { useState } from 'react';

import '../css/components/switch.css';

const FIRST_BUTTON = 0;
const SECOND_BUTTON = 1

function Switch (props) {
  const [ switchedElement, setSwitchedElement ] = useState(0);
  return (
    <div className="Switch">
      <button 
        className={switchedElement == FIRST_BUTTON ? "active" : ""}
        onClick={() => setSwitchedElement(FIRST_BUTTON)}
      >
        {props.items[FIRST_BUTTON]}
      </button>
      <button 
        className={switchedElement == SECOND_BUTTON ? "active" : ""}
        onClick={() => setSwitchedElement(SECOND_BUTTON)}
      >
        {props.items[SECOND_BUTTON]}
      </button>
    </div>
  )
}

export default Switch;