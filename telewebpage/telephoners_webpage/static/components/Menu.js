import React from 'react';

import '../css/components/menu.css';

import logo from '../img/telephoners_logo.svg'

function Menu(props) {
  return (
    <div className="Menu">
      <a href="/">
        <img className="Menu__logo" src={logo} alt="logo" />
      </a>
      <div className="Menu__links">
        <a className="link" href="about">o nas</a>
        <a className="link" href="projects">projekty</a>
        <a className="link" href="work_with_us">współpraca</a>
        <a className="link" href="contact">kontakt</a>
        <a className="Cta" href="accounts/login/">zaloguj się</a>
      </div>
    </div>
  )
}

export default Menu;