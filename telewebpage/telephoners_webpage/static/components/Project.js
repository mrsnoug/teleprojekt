import React from 'react';

import '../css/components/project.css';

import LinkButton from './LinkButton';

function Project({name, desc, short_desc, slug}) {
  return(
    <div className="Project">
      <div className="Project__desctiption">
        <h2>{name}</h2>
        <span className="decription__tag">projekt</span>
        <p>{short_desc.length > 200 ? `${short_desc.substring(0, 350)}...` : short_desc}</p>
      </div>
      <div className="Project__photo">
        <div dangerouslySetInnerHTML={{ __html: desc }} />
        <LinkButton to={`/projects/${slug}`} value="czytaj dalej" class="Project__link"/>
      </div>
    </div>
  )
}

export default Project