import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import "../css/components/slider.css"

import photo1 from "../img/project_1.png";

class Slider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            prevDisable: true,
            nextDisable:
                this.refs && this.refs.offsetWidth >= this.refs.scrollWidth ? true : false,
            descId: 1,
            visible: 1,
            activeItem: "",
            enable: 0,
        };
        this.handleClick = this.handleClick.bind(this);
        this.checkButtons = this.checkButtons.bind(this);

    }

    componentDidMount() {
        this.checkButtons(this.refs.offsetWidth, this.refs.scrollWidth);
    }

    checkButtons(offsetWidthValue, scrollWidthValue) {
        this.setState({
            prevDisable: this.refs.scrollLeft <= 0 ? true : false,
            nextDisable:
                this.refs.scrollLeft + offsetWidthValue >= scrollWidthValue ? true : false
        });
    };

    handleClick(value) {
        this.activeItem = "child__active child"
        console.log(value.id)
    }

    render() {
        const offsetWidthValue = this.refs.offsetWidth,
            scrollWidthValue = this.refs.scrollWidth;
        console.log(offsetWidthValue);
        return (
            <div className="slider">
                <h1>LATA DZIAŁALNOŚCI</h1>
                <div className="slider__navbar">
                    <div
                        className="slider-container"
                        ref={el => {
                            this.refs = el;
                        }}
                    >
                        <div className="slider-wrapper" >
                            {this.props.data.map(value => {
                                return (
                                    <div onClick={() => {
                                        this.setState({ descId: value.id });
                                        this.setState({ visible: value.id });
                                        console.log(this.state.visible)
                                    }}
                                        key={value.id} className={`child ${this.state.visible === value.id ? "child__active" : ""}`}>
                                        <h2>{value.name}</h2>
                                    </div>
                                );
                            })}
                        </div>

                    </div>
                    <div
                        className={`btn prev ${this.state.enable === 0 ? "disable" : ""}`}
                        //disabled={this.state.prevDisable}
                        onClick={() => {
                            this.refs.scrollLeft -= offsetWidthValue / 2;
                            this.checkButtons(offsetWidthValue, scrollWidthValue);
                            this.state.enable -= 1;
                        }}
                    >
                        {"<"}
                    </div>
                    <div
                        className={`btn next ${this.state.enable * this.refs.scrollLeft >= scrollWidthValue ? "disable" : ""}`}
                        //disabled={this.state.nextDisable}
                        onClick={() => {
                            this.refs.scrollLeft += offsetWidthValue / 2;
                            this.checkButtons(offsetWidthValue, scrollWidthValue);
                            this.state.enable += 1;
                        }}
                    >
                        {">"}
                    </div>

                </div>
                <div className="Slider__content slider__active" >
                    <div className="slider__content">
                        <div className="content__text">
                            <h1>ROK {this.props.data[this.state.descId - 1].name}: UKOŃCZENIE PROJEKTU {this.props.data[this.state.descId - 1].desc}</h1>
                            <p>Koło Naukowe TELEPHONERS tworzy grupa studentów studiujących na kierunkach Teleinformatyka oraz Elektronika i Telekomunikacja na Wydziale Informatyki, Elektroniki i Telekomunikacji Akademii Górniczo-Hutniczej im. Stanisława Staszica w Krakowie. Studenci KN Telephoners wspólnie stwarzają sobie możliwości rozwijania własnych zainteresowań oraz realizowania unikalnych.</p>
                        </div>
                    </div>
                    <div className="slider__image">
                        <img src={photo1} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Slider;