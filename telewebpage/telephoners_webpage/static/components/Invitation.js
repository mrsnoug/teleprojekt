import React from 'react';

import '../css/components/invitation.css';

import LinkButton from './LinkButton';

function Invitation({question, linkValue, link}) {
  return(
    <div className="Invitation">
      <h2>{question}</h2>
      <LinkButton class="Invitation__button" to={link} value={linkValue} />
    </div>
  )
}

export default Invitation