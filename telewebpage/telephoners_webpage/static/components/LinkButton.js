import React from 'react';

import '../css/components/buttons.css';

import vector from '../img/vector.svg';

function LinkButton(props){
  return <a className={`Cta--link ${props.class ? props.class : ''}`} href={props.to}>
          {props.value}
          <img src={vector} alt="vector" />
        </a>
}

export default LinkButton;