import React from 'react';

import facebook from '../img/facebook.svg';
import linkedin from '../img/linkedin.svg';
import ITberries from '../img/IT_Berries.svg';

import '../css/components/footer.css';

function Footer (){
  return (
    <div className="Footer">
      <div className="Footer__content">
        <div className="Footer__contact">

          <h2>Kontakt:</h2>
          <a href="mailto:mail@agh.edu.pl">mail@agh.edu.pl</a>
          <a href="tel:997-997-997">997-997-997</a>

          <h2>Inne media:</h2>
          <div className="contact__socialMedia">
            <a href="https://www.facebook.com/telephoners/">
              <img src={facebook} alt="facebook"/>
            </a>
            <a href="https://www.linkedin.com/groups/4140940/">
              <img src={linkedin} alt="linkedin"/>
            </a>
          </div>
        </div>

        <div className="Footer__nav">
          <h2>Nawigacja</h2>
          <div className="nav__data">
            <div>
              <a href="about">O nas</a>
              <a href="projects">Projekty</a>
              <a href="work_with_us">Współpraca</a>
              <a href="news">Aktualności</a>
            </div>
            <div>
              <a href="history">Historia Koła</a>
              <a href="management">Zarząd</a>
            </div>
            <div>
              <a href="docs">Dokumenty</a>
              <a href="user">Panel Użytkownika</a>
            </div>
          </div>
        </div>
      </div>
      
      <div className="Footer__info">
        <p>&copy;2020 SKN TELEPHONERS. All rights reserved</p>
        <div>
          <p>Zaprojektowane przez</p>
          <img src={ITberries} alt="IT Berries"/>
        </div>
      </div>
    </div>
  )
}

export default Footer;