import React from 'react';

import '../css/components/clippedImage.css';

function ClippedImage(props){
  return(
    <img src={props.src} alt={props.alt} className={`ClippedImage ${props.class ? props.class : ''}`}/>
  )
}

export default ClippedImage;