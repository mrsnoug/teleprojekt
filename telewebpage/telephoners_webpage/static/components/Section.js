import React from 'react';

import '../css/components/section.css';

//required props soon
//for now use: first on page : order=first
//             even section: order=even
//             odd section: order=odd

function Section (props) {
  return (
    <div className={`Section Section--${props.order}`}>
      <div className={`Section__content ${props.class ? props.class : ''}`}>
        {props.children}
      </div>
    </div>
  )
}

export default Section;