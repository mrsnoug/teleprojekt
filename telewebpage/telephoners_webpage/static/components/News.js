import React from 'react';

import '../css/pages/newsPage.css';

import LinkButton from './LinkButton';

function News({title, published, body, slug}) {
  console.log(body);
  return(
    <div className='news__content'>
      <div className="news__date">
        <h2>{(published).slice(8, 10)}.{(published).slice(5, 7)}.{(published).slice(0, 4)}</h2>
      </div>
      <div className="news__text">
        <div className="news__title">
          <h2>{title}</h2>
          <LinkButton to={`/news/${slug}`} value="czytaj dalej" />
        </div>
        <div className="news__body" dangerouslySetInnerHTML={{ __html: `${body.substring(0, 200)}...` }} >
        </div>
      </div>
    </div>
  )
}

export default News