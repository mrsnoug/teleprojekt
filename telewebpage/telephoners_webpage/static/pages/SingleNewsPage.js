import React from 'react';
import ReactDOM from 'react-dom';

import '../css/pages/newsPage.css';

import Menu from '../components/Menu';
import Section from '../components/Section';
import Footer from '../components/Footer';

function SingleNewsPage() {
  const {title, published, body} = JSON.parse(window.props);

  return(
    <div className="SingleNewsPage">
      <Menu />
      <Section order='first'>
        {title ? 
          <div className="SingleNewsPage__content">
            <h1>{title}</h1>
            <p className='date'>{(published).slice(8, 10)}.{(published).slice(5, 7)}.{(published).slice(0, 4)}</p>
            <div dangerouslySetInnerHTML={{ __html: body }} /> 
          </div>
          : 'nie znaleziono aktualności'
        }
      </Section>
      <Footer />
    </div>
  )
}

ReactDOM.render(
  React.createElement(SingleNewsPage, window.props),
  document.getElementById('singleNews')
)