import React from 'react';
import ReactDOM from 'react-dom';

import '../css/pages/loginPage.css';

import Menu from '../components/Menu';
import Section from '../components/Section';
import Footer from '../components/Footer';
import LinkButton from '../components/LinkButton';

function LoginPage() {
    return (
        <div className="LoginPage">
            <Menu />
            <div className="login__content">
                <h1>LOGOWANIE</h1>
                <div className="login__inputs">
                    <div className="login__input">
                        <input type="text" id="login" name="login" className="input" placeholder="Login" />
                    </div>
                    <div className="login__input">
                        <input type="text" id="password" name="password" className="input" placeholder="Hasło" />
                    </div>
                    <label className="login__checkbox">
                        <h2>Zapamiętaj mnie</h2>
                        <input type="checkbox" />
                        <span class="checkmark" />
                    </label>
                    <div className="login__button">
                        <LinkButton to='' value='ZALOGUJ SIĘ' />
                    </div>
                </div>
            </div>
        </div>
    )
}

ReactDOM.render(
    <LoginPage />,
    document.getElementById('login_page')
);