import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Menu from '../components/Menu';
import Footer from '../components/Footer';
import Section from '../components/Section';
import Invitation from '../components/Invitation';

import AVSystem from '../img/współpraca/AVSystem.png';
import botland from '../img/współpraca/botland.png';
import cisco from '../img/współpraca/cisco.png';
import nokia from '../img/współpraca/nokia.png';
import nordic from '../img/współpraca/nordic.png';
import proidea from '../img/współpraca/proidea.jpg';
import samsung from '../img/współpraca/SAMSUNG.png';
import solarPlane from '../img/współpraca/SolarPlane.png';

import '../css/pages/coopPage.css';

class CoopPage extends React.Component {
    constructor() {
        super();
        this.state = {
            partners: [
                {
                    name: 'AVSystems',
                    img: AVSystem,
                    link: 'https://www.avsystem.com/'
                }, {
                    name: 'Botland',
                    img: botland,
                    link: 'https://botland.com.pl/pl/'
                }, {
                    name: 'Cisco',
                    img: cisco,
                    link: 'https://www.cisco.com/c/pl_pl/index.html'
                }, {
                    name: 'Nokia',
                    img: nokia,
                    link: 'https://www.nokia.com/pl_pl/'
                }, {
                    name: 'Nordic Semiconductor',
                    img: nordic,
                    link: 'https://www.nordicsemi.com/'
                }, {
                    name: 'Proidea',
                    img: proidea,
                    link: 'https://proidea.pl/'
                }, {
                    img: samsung,
                    link: 'https://www.samsung.com/pl/'
                }, {
                    img: solarPlane,
                    link: 'http://www.solarplane.agh.edu.pl/'
                }
            ]
        }
    }
    render() {
        return (
            <div className="CoopPage">
                <Menu />
                <Section className="CoopPage__content" order='first'>
                    <div className="cooperation">
                        <h1>WSPÓŁPRACA</h1>
                        <div className="cooperator__list">
                            {this.state.partners.map((partner, i) => {
                                return (
                                    <a key={i} href={partner.link} target="_blank" className="cooperator__element">
                                        <img src={partner.img} className="cooperator__img" />
                                    </a>
                                )
                            })}
                        </div>
                    </div>
                    <Invitation question="Zainteresowany współpracą?" link="/contact" linkValue="skontaktuj się z nami" />
                </Section>

                <Footer />
            </div>
        )
    }
}

ReactDOM.render(
    <CoopPage />,
    document.getElementById('coop')
);