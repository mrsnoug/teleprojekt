import React from 'react';
import ReactDOM from 'react-dom';

import aboutMePhoto from '../img/aboue_me_image.png';
import ourPhoto from '../img/our_photo.png';
import project from '../img/project_1.png';
import sliderArrow from '../img/slider_arrow.svg';

import '../css/pages/aboutMePage.css';

import Menu from '../components/Menu';
import Footer from '../components/Footer';
import Section from '../components/Section';
import ClippedImage from '../components/ClippedImage';
import LinkButton from '../components/LinkButton';

class AboueMePage extends React.Component {
    render() {
        return (
            <div className="AboueMePage">
                <Menu />

                <Section order='first'>
                    <div className="aboutMe__content">
                        <h1>O nas</h1>
                        <p>Koło Naukowe TELEPHONERS tworzy grupa studentów studiujących na kierunkach Teleinformatyka
                        oraz Elektronika i Telekomunikacja na Wydziale Informatyki, Elektroniki i Telekomunikacji Akademii
                        Górniczo-Hutniczej im. Stanisława Staszica w Krakowie. Studenci KN Telephoners wspólnie stwarzają
                sobie możliwości rozwijania własnych zainteresowań oraz realizowania unikalnych.</p>
                    </div>
                    <ClippedImage class="aboutMe__image" src={aboutMePhoto} alt="zdjęcie" />
                </Section>
                <Section className="AboutMePage__content" order="odd">
                    <div className="AboutMe__list">
                        <div className="AboutMe__element">
                            <h2> &#128347; HISTORIA SUKCESÓW</h2>
                            <p>
                                Koło Naukowe TELEPHONERS tworzy grupa studentów studiujących na kierunkach Teleinformatyka
                                oraz Elektronika i Telekomunikacja na Wydziale Informatyki, Elektroniki i Telekomunikacji Akademii
                                Górniczo-Hutniczej im. Stanisława Staszica w Krakowie. Studenci KN Telephoners wspólnie stwarzają
                                sobie możliwości rozwijania własnych zainteresowań oraz realizowania unikalnych.
                        </p>
                        </div>
                        <div className="AboutMe__element">
                            <h2>&#129351; WSPANIALI LUDZIE</h2>
                            <p>
                                Koło Naukowe TELEPHONERS tworzy grupa studentów studiujących na kierunkach Teleinformatyka
                                oraz Elektronika i Telekomunikacja na Wydziale Informatyki, Elektroniki i Telekomunikacji Akademii
                                Górniczo-Hutniczej im. Stanisława Staszica w Krakowie. Studenci KN Telephoners wspólnie stwarzają
                                sobie możliwości rozwijania własnych zainteresowań oraz realizowania unikalnych.
                        </p>
                        </div>
                        <div className="AboutMe__element">
                            <h2> &#10084; WSPANIAŁY PREZES</h2>
                            <p>
                                Koło Naukowe TELEPHONERS tworzy grupa studentów studiujących na kierunkach Teleinformatyka
                                oraz Elektronika i Telekomunikacja na Wydziale Informatyki, Elektroniki i Telekomunikacji Akademii
                                Górniczo-Hutniczej im. Stanisława Staszica w Krakowie. Studenci KN Telephoners wspólnie stwarzają
                                sobie możliwości rozwijania własnych zainteresowań oraz realizowania unikalnych.
                        </p>
                        </div>
                    </div>
                    <LinkButton class="AboutMe__button" to='/history' value='historia koła' />
                </Section>


                <Footer />
            </div>
        );
    }
}

ReactDOM.render(
    <AboueMePage />,
    document.getElementById('about_us')
);