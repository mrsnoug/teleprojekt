import React from 'react';
import ReactDOM from 'react-dom';

import '../css/pages/contactPage.css';

import Menu from '../components/Menu';
import Section from '../components/Section';
import Footer from '../components/Footer';
import LinkButton from '../components/LinkButton';

function ContactPage() {
    return (
        <div className="ContactPage">
            <Menu />
            <Section order="first">
                <div className="contact__content">
                    <h1>KONTAKT</h1>
                    <div className="contact__form">
                        <div className="contact__theme">
                            <label for="theme"><h2>Temat*</h2></label>
                            <input type="text" id="theme" name="theme" className="theme__input input" placeholder="Temat" />
                        </div>
                        <div className="contact__body">
                            <label for="body"><h2>Treść*</h2></label>
                            <textarea id="body" name="body" className="body__input input" placeholder="Treść"></textarea>
                        </div>
                        <div className="contact__email">
                            <label for="email"><h2>Email*</h2></label>
                            <input type="text" id="email" name="email" className="email__input input" placeholder="Email" />
                        </div>
                        <div className="contact__checkboxes">
                            <label className="contact__checkbox">
                                <h2>Akceptuję RODO*</h2>
                                <input type="checkbox" />
                                <span class="checkmark" />
                            </label>
                            <label className="contact__checkbox">
                                <h2>Nie jestem robotem*</h2>
                                <input type="checkbox" />
                                <span class="checkmark" />
                            </label>
                            <label className="contact__checkbox">
                                <h2>Wyślij kopie do mnie</h2>
                                <input type="checkbox" />
                                <span class="checkmark" />
                            </label>
                        </div>
                        <div className="contact__button">
                            <LinkButton to='/news' value='wyślij' />
                        </div>
                    </div>
                </div>
            </Section>
            <Footer />
        </div>
    )
}

ReactDOM.render(
    <ContactPage />,
    document.getElementById('contact_page')
);