import React from 'react';
import ReactDOM from 'react-dom';

import newsPhoto from '../img/1.png';
import ourPhoto from '../img/our_photo.png';
import project from '../img/project_1.png';
import sliderArrow from '../img/slider_arrow.svg';

import '../css/pages/landingPage.css';

import Menu from '../components/Menu';
import Footer from '../components/Footer';
import Section from '../components/Section';
import ClippedImage from '../components/ClippedImage';
import LinkButton from '../components/LinkButton';


function LandingPage() {
  console.log(window.props)
  const projects = JSON.parse(window.props.projects);
  const news = JSON.parse(window.props.news);
  console.log(projects, news);

  return (
    <div className="LandingPage" >
      <Menu />
      <Section class="LandingPage__news" order='first'>
        <ClippedImage class="news__image" src={newsPhoto} alt="zdjęcie" />
        <div className="news__content">
          <h1>{news[0].title}</h1>
          <h2>5 Maja 2020</h2>
          <p>{`${news[0].body.substring(0, 350)}...`}</p>
        </div>
        <div className='news__opers'>
          <div className="news__pagination">
            <span></span>
            <span></span>
            <span></span>
          </div>
          <LinkButton to='/news' value='czytaj dalej' />
        </div>
      </Section>
      <Section class="LandingPage_aboutUs" order="even">
        <div className="aboutUs__content">
          <h1>O nas</h1>
          <p>Koło Naukowe TELEPHONERS tworzy grupa studentów studiujących na kierunkach Teleinformatyka
          oraz Elektronika i Telekomunikacja na Wydziale Informatyki, Elektroniki i Telekomunikacji Akademii
          Górniczo-Hutniczej im. Stanisława Staszica w Krakowie. Studenci KN Telephoners wspólnie stwarzają
              sobie możliwości rozwijania własnych zainteresowań oraz realizowania unikalnych.</p>
        </div>
        <img className="aboutUs__image" src={ourPhoto} alt="Nasze zdjęcie" />
        <div className="aboutUs__linkBox">
          <LinkButton to='/about' value='poznaj nas lepiej' />
        </div>
      </Section>
      <Section class="LandingPage__projects" order="odd">
        <div className="projects__slider">
          {/* <button className="slider__arrow slider__arrow--prev">
            <img src={sliderArrow} alt="strzałka" />
          </button> */}
          {/* <div className="slider__element">
            <img src={project} alt="projekt" />
          </div>
          <div className="slider__element">
            <img src={project} alt="projekt" />
          </div>
          <div className="slider__element">
            <img src={project} alt="projekt" />
          </div> */}
          {projects.slice(0, 3).map((project, i) => {
            return(
              <a key={i}
              href={`/${project.name.replace(' ', '_')}`} 
              className="slider__element" 
              dangerouslySetInnerHTML={{ __html: project.desc }} />
            )
          })}
          {/* <button className="slider__arrow slider__arrow--next">
            <img src={sliderArrow} alt="strzałka" />
          </button> */}
        </div>
        <LinkButton to="/projects" value="zobacz wszystkie projekty" class="projects__link" />
      </Section>
      <Footer />
    </div>
  );
}
ReactDOM.render(
  React.createElement(LandingPage, window.props),
  document.getElementById('landing_page')
);

