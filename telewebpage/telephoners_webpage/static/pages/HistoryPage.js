import React from 'react';
import ReactDOM from 'react-dom';

import newsPhoto from '../img/1.png';
import ourPhoto from '../img/our_photo.png';
import project from '../img/project_1.png';
import sliderArrow from '../img/slider_arrow.svg';

import '../css/pages/landingPage.css';
import '../css/pages/historyPage.css';

import Menu from '../components/Menu';
import Footer from '../components/Footer';
import Section from '../components/Section';
import ClippedImage from '../components/ClippedImage';
import LinkButton from '../components/LinkButton';
import Slider from "../components/Slider";

class HistoryPage extends React.Component {
    constructor() {
        super();
        this.state = {
            slider: [
                { id: 1, name: "2019", desc: 'XD9' },
                { id: 2, name: "2018", desc: 'XD8' },
                { id: 3, name: "2017", desc: 'XD7' },
                { id: 4, name: "2016", desc: 'XD6' },
                { id: 5, name: "2015", desc: 'XD4' },
                { id: 6, name: "2014", desc: 'XD3' },
                { id: 7, name: "2014", desc: 'XD3' },
                { id: 8, name: "2014", desc: 'XD3' },
                { id: 9, name: "2014", desc: 'XD3' },
                { id: 10, name: "2014", desc: 'XD3' },
                { id: 11, name: "2014", desc: 'XDXDD' },
                { id: 12, name: "2014", desc: 'XD123' },
                { id: 13, name: "2014", desc: 'XD0' },

            ]
        }
    }
    render() {
        return (
            <div className="HistoryPage">
                <Menu />
                <Section class="HistoryPage__content" order='first-twist'>
                    <div className="history__content">
                        <h1>Historia koła</h1>
                        <h2>x lat sukcesów</h2>
                        <p>Koło Naukowe TELEPHONERS tworzy grupa studentów studiujących na kierunkach Teleinformatyka
                        oraz Elektronika i Telekomunikacja na Wydziale Informatyki, Elektroniki i Telekomunikacji Akademii
                        Górniczo-Hutniczej im. Stanisława Staszica w Krakowie. Studenci KN Telephoners wspólnie stwarzają
                sobie możliwości rozwijania własnych zainteresowań oraz realizowania unikalnych.</p>
                    </div>
                    <ClippedImage class="history__image" src={newsPhoto} alt="zdjęcie" />
                    <div className='news__opers'>
                        <div className="news__pagination">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </Section>
                <Section class="LandingPage_aboutUs" order="odd">
                    <Slider data={this.state.slider} />
                    <LinkButton class="HistoryPage__button" to='/management' value='ZARZĄD KOŁA' />
                </Section>
                <Footer />
            </div>
        );
    }
}

ReactDOM.render(
    <HistoryPage />,
    document.getElementById('history')
);