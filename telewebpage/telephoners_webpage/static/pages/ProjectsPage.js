import React from 'react';
import ReactDOM from 'react-dom';

import '../css/pages/projectsPage.css';

import Menu from '../components/Menu';
import Section from '../components/Section';
import Footer from '../components/Footer';
import Project from '../components/Project';
import Invitation from '../components/Invitation';

function ProjectsPage() {
  const projects = JSON.parse(window.props);
  console.log('aaa', projects);
  return(
    <div className="ProjectsPage">
      <Menu />
      <Section order='first'>
        <h1>Nasze projekty</h1>
        {projects.map((project, i) => <Project key={i} {...project} />)}

        <Invitation question="Podobają ci się nasze projekty?" link="/contact" linkValue="dołącz do nas" />

      </Section>
      <Footer />
    </div>
  )
}

ReactDOM.render(
  React.createElement(ProjectsPage, window.props),
  document.getElementById('projects')
)