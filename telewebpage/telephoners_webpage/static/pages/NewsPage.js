import React from 'react';
import ReactDOM from 'react-dom';

import '../css/pages/newsPage.css';

import Menu from '../components/Menu';
import Section from '../components/Section';
import Footer from '../components/Footer';
import News from '../components/News';

import arrow from "../img/vector.svg";

class NewsPage extends React.Component {
  constructor() {
    super();
    this.state = {
      newsData: JSON.parse(window.props),
    }
  }

  render() {

    let { clicked, idClicked, newsData } = this.state;
    return (
      <div className="NewsPage">
        <Menu />
        <Section order='first'>
          <div className="news__list">
            <h1>AKTUALNOŚCI</h1>
            {newsData.map((news, id) => {
              return (
                <News key={id} {...news} />
              )
            })}
          </div>
        </Section>
        <Footer />
      </div>
    );
  }
}

ReactDOM.render(
  React.createElement(NewsPage, window.props),
  document.getElementById('news_page')
);

