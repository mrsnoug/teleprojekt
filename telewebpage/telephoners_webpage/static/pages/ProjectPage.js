import React from 'react';
import ReactDOM from 'react-dom';

import '../css/pages/projectPage.css';

import Menu from '../components/Menu';
import Section from '../components/Section';
import Footer from '../components/Footer';
import Invitation from '../components/Invitation';

function ProjectsPage() {
  const {name, desc, short_desc} = JSON.parse(window.props);

  return(
    <div className="ProjectPage">
      <Menu />
      <Section order='first'>
        {name ? 
          <div className="ProjectPage__content">
            <h1>{name}</h1>
            <div dangerouslySetInnerHTML={{ __html: desc }} /> 
            <p>{short_desc}</p>
            <Invitation question="Masz więcej pytań odnośnie naszych projektów?" link="/contact" linkValue="napisz do nas" />
          </div>
          : 'nie znaleziono projektu'
        }
        

      </Section>
      <Footer />
    </div>
  )
}

ReactDOM.render(
  React.createElement(ProjectsPage, window.props),
  document.getElementById('project')
)