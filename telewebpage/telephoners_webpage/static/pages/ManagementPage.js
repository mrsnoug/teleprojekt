import React from 'react';
import ReactDOM from 'react-dom';

import '../css/pages/managementPage.css';

import manager1 from '../img/zdjecie-arek.jpg'
import facebook from '../img/facebook.svg';
import linkedin from '../img/linkedin.svg';

import Menu from '../components/Menu';
import Section from '../components/Section';
import Footer from '../components/Footer';

function ManagementPage() {
    return ( 
    <div className="ManagementPage">
      <Menu />
      <Section class="ManagementPage__content" order='first'>
        <h1>Zarząd koła</h1>
        <div className="ManagementPage__people">

          <div className="person">
            <img className="person__photo" alt="Arkadiusz Pajor" src={manager1}/>
            <h2>Arkadiusz Pajor</h2>
            <p>Koło Naukowe TELEPHONERS tworzy grupa studentów studiujących na kierunkach Teleinformatyka oraz Elektronika i Telekomunikacja na Wydziale Informatyki, Elektroniki i Telekomunikacji Akademii Górniczo-Hutniczej im. S</p>
            <div className="person_social">
              <a href="https://www.facebook.com/telephoners/">
                <img src={facebook} alt="facebook"/>
              </a>
              <a href="https://www.linkedin.com/groups/4140940/">
                <img src={linkedin} alt="linkedin"/>
              </a>
            </div>
          </div>

          <div className="person">
            <img className="person__photo" alt="Arkadiusz Pajor" src={manager1}/>
            <h2>Arkadiusz Pajor</h2>
            <p>Koło Naukowe TELEPHONERS tworzy grupa studentów studiujących na kierunkach Teleinformatyka oraz Elektronika i Telekomunikacja na Wydziale Informatyki, Elektroniki i Telekomunikacji Akademii Górniczo-Hutniczej im. S</p>
            <div className="person_social">
              <a href="https://www.facebook.com/telephoners/">
                <img src={facebook} alt="facebook"/>
              </a>
              <a href="https://www.linkedin.com/groups/4140940/">
                <img src={linkedin} alt="linkedin"/>
              </a>
            </div>
          </div>

        </div>
      </Section>
      <Footer />
    </div>
  )
}

ReactDOM.render(
  <ManagementPage />,
  document.getElementById('management')
)