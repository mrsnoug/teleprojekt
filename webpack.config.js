const path = require("path");
const BundleTracker = require('webpack-bundle-tracker');
const getFilesFromDir = require("./config/files");

const PAGE_DIR = "telewebpage/telephoners_webpage/static/pages";

const jsFiles = getFilesFromDir(PAGE_DIR, [".js"]);
const entry = jsFiles.reduce((obj, filePath) => {
  const entryChunkName = filePath.replace(path.extname(filePath), "").replace(`${PAGE_DIR}/`, "");
  obj[entryChunkName] = `./${filePath}`;
  return obj;
}, {});

module.exports = {
  entry: entry,

  output: {
    path: path.resolve('./telewebpage/telephoners_webpage/static/bundles/'),
    filename: "[name]-[hash].js",
  },

  plugins: [
    new BundleTracker({
      filename: './telewebpage/webpack-stats.json'
    }),
  ],
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ['url-loader'],
      },
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  }

};