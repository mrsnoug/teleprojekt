# Telewebpage

## Pre-running and installation

```bash
sudo apt-get update
sudo apt-get install libpq-dev postgresql postgresql-contrib

sudo su - postgres
psql
CREATE DATABASE postgres;
CREATE USER admin WITH PASSWORD 'admin';
ALTER ROLE admin SET client_encoding TO 'utf8';
ALTER ROLE admin SET default_transaction_isolation TO 'read committed';
ALTER ROLE amdin SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE postgres TO admin;
\q
exit

python3 -m venv tele_venv
source televenv/bin/activate
pip install -r requirements.txt
```

Running the project:

```bash
python manage.py runserver
```

Webpage will be available at 127.0.0.1:8000

## Client server

```
npm install
npm run watch
```

